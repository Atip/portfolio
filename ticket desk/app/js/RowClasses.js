import Seat from './SeatConstructor';

export class Row {
	constructor( seats, type, number ){
		this.number = Number(number);
		this.seats = [];
	}
}

export class EconomRow extends Row {
	constructor( seats, type, number ){		
		super( seats, type, number );		
		this.number = Number(number);
		this.price = 50;
		this.type = type;
		for (let i = 1; i <= seats; i++){
			let newSeat = new Seat(`r${this.number}s${i}`, i, this.number, type, this.price );
			this.seats.push(newSeat);			
		}
		console.log('Eco row created', this);
	}
}

export class NormalRow extends Row {
	constructor( seats, type, number ){		
		super( seats, type, number );
		this.number = Number(number)
		this.price = 100;
		this.type = type;
		for (let i = 1; i <= seats; i++){
			let newSeat = new Seat(`r${this.number}s${i}`, i, this.number, type, this.price );
			this.seats.push(newSeat);			
		}
		console.log('Normal row created', this);	
	}
}

export class PremiumRow extends Row {
	constructor( seats, type, number ){		
		super( seats, type, number );		
		this.number = Number(number);
		this.price = 150;
		this.type = type;
		for (let i = 1; i <= seats; i++){
			let newSeat = new Seat(`r${this.number}s${i}`, i, this.number, type, this.price );
			this.seats.push(newSeat);			
		}
		console.log('Premium row created', this);
	}
}

export default Row;