const SidebarRender = () =>{
	const contentBox = sideMenu.querySelector('.sideMenu__content');
	const orderArr = JSON.parse( localStorage.getItem('order') );
	if ( orderArr.length !== 0 ){		
		contentBox.innerHTML = "";
		orderArr.map( item =>{

			contentBox.innerHTML +=
			`
			<p>Ряд: ${item.row}, Место: ${item.num}, Цена: ${item.price}</p>
			`
		});
	} else {
		contentBox.innerHTML =
		`
		В корзине пусто
		`
	}	
}

export default SidebarRender;
