import Row from './RowClasses';
import Decorator from './Decorator';
import Modal from './Modal';
import SidebarRender from './Sidebar';
import cartCalc from './CartCalc';
import timer from './Timer';
import { obsrvbl } from './CartAdd';
import ObservableDelete from './CartDelete';

let number = 0;
let orderArr;
	if(localStorage.getItem('order') ){		
		orderArr = JSON.parse( localStorage.getItem('order') );		
	} else {
		orderArr = [];
	}

export class Cinema {
	constructor(){
		if ( localStorage.getItem('rows') ){
			this.rowsArray = [];
			this.rowsArray = JSON.parse( localStorage.getItem('rows') );
			number = this.rowsArray[this.rowsArray.length-1].number;
		} else {
			this.rowsArray = []; //Здесь будет массив с нашими рядами	
		}		
		this.render = this.render.bind(this); //Рендер
		this.addRow = this.addRow.bind(this); //Добавить ряд
	}

	addRow( seats, type='normal' ){
		number++ //увеличиваем номер ряда(его id) на 1
		
		if ( this.rowsArray.length < 2 ){ // Первые два по-умолчанию "эконом"
			type = 'econom';
		}

		let row = new Decorator( seats, type, number ); //Отправляем в декоратор
		this.rowsArray.push( row ); //добавляем в массив с рядами, который ниже записывается в Лоакл сторедж
		localStorage.setItem( `rows`, JSON.stringify( this.rowsArray ) );		
		this.render();
	}

	render() {
		const root = document.getElementById('root');
		//Рисуем ряды
		if ( localStorage.getItem('rows') ){ //Если есть в Локал сторедж		
			let ArrayFromLS = JSON.parse( localStorage.getItem(`rows`) );
			root.innerHTML = "";
			ArrayFromLS.map(row => {
				const wrap = document.createElement('div');			
				const div = document.createElement('div');
				const divNum = document.createElement('div');
					  div.classList.add('row');
					  wrap.classList.add('row__wrap');
					  divNum.innerHTML = `<div class="row__number">Ряд ${row.number}:</div>`;
					  wrap.appendChild(divNum);
					  wrap.appendChild(div);

					for(let seat in row.seats){
						let block = document.createElement('div'); //Создаём обёртку для кнопки, чтобы навесить обработчик					
						block.innerHTML +=
						`
							<button class="row__seat ${row.type} ${row.seats[seat].status}" id="${row.seats[seat].id}" data-status="${row.seats[seat].status}">${row.seats[seat].SeatNum}</button>			
						`									
						div.appendChild(block);
						//Кнопка на каждое место для заказа
						let rowType = row.type;
						let seatNum = row.seats[seat].SeatNum;
						let rowNum = row.number;
						let seatPrice = row.price;
						let seatId = row.seats[seat].id;
						let seatStatus = row.seats[seat].status;
					
						let btn = div.querySelector(`.row__seat[id="${row.seats[seat].id}"]`);
							btn.addEventListener('SeatBook', (e) => { obsrvbl.sendMessage(e, seatNum, row.number, seatPrice, rowType, seatId) }); //Добавляем ивент, чтобы прокинуть параметры
							btn.addEventListener('SeatCancel', (e) => { ObservableDelete.sendMessage( e, seatNum, row.number, seatPrice, rowType, seatId ) }); //Добавляем ивент, чтобы прокинуть параметры
							btn.addEventListener('click', (e) => {
								if (e.target.dataset.status === 'available'){
									let SeatBook = new Event('SeatBook', {"bubbles":true, "cancelable":false});							
									btn.dispatchEvent(SeatBook)
									e.target.dataset.status = 'booked';
								} else {
									let SeatCancel = new Event('SeatCancel', {"bubbles":true, "cancelable":false});							
									btn.dispatchEvent(SeatCancel)	
									e.target.dataset.status = 'available';
								}							
							});						
					};
				//Кнопка для добавления рядов
				if (ArrayFromLS.indexOf(row) === ArrayFromLS.length - 1){
						let addBtn = document.createElement('button');
							addBtn.id = "addRow";
							addBtn.innerHTML = "+";
							addBtn.dataset.type = "add";
							//Открываем модальное окно
							addBtn.addEventListener('click', (e) =>{
								Modal(e);
								const modalSaveBtn = document.getElementById('modalBtn');
								const NumOfSeats = document.getElementById('NumOfSeats');
								const selectType = document.getElementById('selectType');
								const warning = document.createElement('p');
									modalSaveBtn.addEventListener('click', () =>{
										if (NumOfSeats.value > 15 || NumOfSeats.value < 1){ //Проверка и предупреждение от 1 до 15 мест
											
											warning.innerHTML = 'Кол-во место должно быть от 1 до 15';
											warning.style.color = 'red';
											NumOfSeats.after(warning);
										} else {
											if (warning){
												warning.innerHTML = "";
												warning.remove();
											}
											this.addRow(NumOfSeats.value, selectType.value);										
										}
										
									});
							});
						wrap.appendChild(addBtn);
					}
				//Кнопка удаления ряда					
					let deleteRowBtn = document.createElement('button');
						deleteRowBtn.id = 'deleteRowBtn';
						deleteRowBtn.innerHTML = '-';							
						deleteRowBtn.dataset.row = `${row.number}`;							
						deleteRowBtn.addEventListener('click', (e) => {
							ArrayFromLS.map(row => {															
								if ( row.number === Number(e.target.dataset.row) ){
									let idx = ArrayFromLS.indexOf(row);
									let idx2 = this.rowsArray.indexOf(row);
									ArrayFromLS.splice(idx, 1);
									this.rowsArray.splice(idx2, 1);
									number--									
									
									if (ArrayFromLS.length !== 0){
										localStorage.setItem('rows', JSON.stringify( ArrayFromLS ) );										
									} else {
										localStorage.removeItem('rows');
										this.rowsArray = [];
									}
									this.render()																
								}
							});
						});
					wrap.appendChild(deleteRowBtn);
				root.appendChild(wrap);
			});
		} else { //Кнопка для добавления рядов если нет ни одного ряда			
			root.innerHTML = `<button id="addRow">Добавить ряд</button>`
			let addBtn = document.getElementById('addRow');
				addBtn.addEventListener('click', (e) =>{
								Modal(e);
								const modalSaveBtn = document.getElementById('modalBtn');
								const NumOfSeats = document.getElementById('NumOfSeats');
								const selectType = document.getElementById('selectType');
								const warning = document.createElement('p');
								modalSaveBtn.addEventListener('click', () =>{
									if (NumOfSeats.value > 15 || NumOfSeats.value < 1){ //Проверка и предупреждение от 1 до 15 мест
										warning.innerHTML = 'Кол-во место должно быть от 1 до 15';
										warning.style.color = 'red';
										NumOfSeats.after(warning);
									} else {
										if (warning){
											warning.innerHTML = "";
											warning.remove();
										}
										this.addRow(NumOfSeats.value, selectType.value);										
									}
									
								});
							});
				root.appendChild(addBtn);
		}
		//Если в локал сторедже есть активные некупленные билеты, тогда показывать их после перезагрузки страницы
		if( localStorage.getItem('order') && JSON.parse( localStorage.getItem('order') ).length !== 0 ){
			const sideMenu = document.getElementById('sideMenu');	
			sideMenu.classList.add('sideMenu__show');
			SidebarRender();
			cartCalc();
			timer();
		}				
	}
}

export {orderArr};
export default Cinema;