// Удаление места из корзины(чтобы удалить нужно кликнуть на место на схеме зала)

import { Observable, Observer } from './Observer';
import {orderArr} from './Cinema';
import cartCalc from './CartCalc';
import SidebarRender from './Sidebar';

const ObservableDelete = new Observable();

const obsDeleteFromLS = new Observer( (e, num, row, price, type, id) =>{
	const contentBox = document.querySelector('.sideMenu__content');
	const orderLS = JSON.parse( localStorage.getItem('order') );
	if ( localStorage.getItem('order') ){		
			orderLS.map( item =>{
				if(item.id === id){
					let idx = orderLS.indexOf(item);
					orderLS.splice(idx, 1);
					localStorage.setItem('order', JSON.stringify(orderLS) )
				}
			});
		SidebarRender();
	}
});

const obsDeleteFromArray = new Observer( (e, num, row, price, type, id) =>{
	orderArr.map(item => {
		if(item.id === id){
			let idx = orderArr.indexOf(item);
			orderArr.splice(idx, 1);			
		}
	});
});

const obsDeleteClass = new Observer( (e, num, row, price, type, id) => {
	e.target.classList.remove('booked');
});

const deleteTimer = new Observer ( (e, num, row, price, type, id) =>{
	const orderTimer = document.getElementById('order__timer');
	const orderArr = JSON.parse( localStorage.getItem('order') );
	if(orderTimer.timer && orderArr.length === 0) {		
		clearInterval(orderTimer.timer);
		orderTimer.innerHTML = "";
	}
});

const deleteSum = new Observer ( (e, num, row, price, type, id) =>{
	cartCalc();
});

//Меняем статус с booked на available
const obsChangeStatus = new Observer ( (e, num, row, price, type, id) => {
	const rowsLS = JSON.parse( localStorage.getItem('rows') );
	rowsLS.map( row => {		
		for (let seat in row.seats){			
			if (row.seats[seat].id === id){
				row.seats[seat].status = 'available';				
			}
		}
	});

	localStorage.setItem('rows', JSON.stringify(rowsLS) )
});

ObservableDelete.addObserver(obsDeleteFromLS);
ObservableDelete.addObserver(obsDeleteFromArray);
ObservableDelete.addObserver(obsDeleteClass);
ObservableDelete.addObserver(deleteTimer);
ObservableDelete.addObserver(deleteSum);
ObservableDelete.addObserver(obsChangeStatus);

export default ObservableDelete;