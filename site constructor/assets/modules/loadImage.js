
/*
Костыль:
<p id="article__image${+key}">${RenderImageFunc(item.image, (error, img)=>{
	        							let target = document.getElementById("article__image" + key);
	        							if(error){
	        								img.src = '../public/img/noimage.jpg'
	        								target.appendChild(img);
	        							}	        							
										target.appendChild(img);
	        						})}</p>
*/


export let RenderImageFunc = (url, callback)=>{
			let anotherImage = new Image()
			anotherImage.setAttribute('height', '200px');
			anotherImage.setAttribute('class', 'article__image');
			
			anotherImage.onload = () =>	callback(null, anotherImage);

			anotherImage.onerror = () => callback(true, anotherImage);
			anotherImage.src = url;			
			}

export default RenderImageFunc;