import {saveToLS} from '../modules/save';

export const popup = (event) => {
			event.preventDefault();
	        const fade = document.createElement('div');
	        const content = document.createElement('div');        
	        const body = document.querySelector('body');
	        const div = document.createElement('div');
	        const nav_item1 = document.getElementById('nav__item-1');
	        const nav_item2 = document.getElementById('nav__item-2');
	        const nav_item3 = document.getElementById('nav__item-3');
	        const nav_item4 = document.getElementById('nav__item-4');
	        const side_item1 = document.getElementById('side__item-1');
	        const side_item2 = document.getElementById('side__item-2');
	        const side_item3 = document.getElementById('side__item-3');
	        const side_item4 = document.getElementById('side__item-4');

	        //Навешиваем классы на наши элементы
	        fade.classList.add('fade');
	        content.classList.add('popup__content');
	        div.classList.add('popup__form');	        
	        fade.appendChild(content)
	        body.appendChild(fade);
	        fade.style.display = 'block'
	        
	        //Закрыть по клике в любом месте окна
	        window.addEventListener('click', event => {
				if (event.target === fade){
					fade.style.display = 'none';
					fade.remove()
				}	
	        })

	        //Определяем какие элементы нам вывести из верхнего меню или нижнего
	        if (event.target.id === 'editNavbar'){
	        	div.innerHTML = `<form name="editItems">
	        						<p>
		        						<label for="nav_item-1">Change text for item 1</label>
		        						<input type="text" name="nav_item-1" placeholder="${nav_item1.innerText}">
		        						<label for="nav_item-1-link">Change link for item 1</label>
		        						<input type="text" name="nav_item-1-link" placeholder="${nav_item1.querySelector("a").href}" data-id="link">
									</p>
									<p>
		        						<label for="nav_item-2">Change text for item 2</label>
		        						<input type="text" name="nav_item-2" placeholder="${nav_item2.innerText}">
		        						<label for="nav_item-2-link">Change link for item 2</label>
		        						<input type="text" name="nav_item-2-link" placeholder="${nav_item2.querySelector("a").href}" data-id="link">
	        						</p>
									<p>
		        						<label for="nav_item-3">Change text for item 3</label>
		        						<input type="text" name="nav_item-3" placeholder="${nav_item3.innerText}">
		        						<label for="nav_item-3-link">Change link for item 3</label>
		        						<input type="text" name="nav_item-3-link" placeholder="${nav_item3.querySelector("a").href}" data-id="link">
	        						</p>
									<p>
		        						<label for="nav_item-4">Change text for item 4</label>
		        						<input type="text" name="nav_item-4" placeholder="${nav_item4.innerText}">
		        						<label for="nav_item-4-link">Change link for item 4</label>
		        						<input type="text" name="nav_item-4-link" placeholder="${nav_item4.querySelector("a").href}" data-id="link">
	        						</p>									
	        						<div class="buttons">
	        						<p>
	        							<a href="#" id="save" class="button save"/>SAVE</a>
	        							<a href="#" id="cancel" class="button cancel"/>CANCEL</a>
	        						</p>
	        						</div>
	        					</form>`
	        	content.appendChild(div);
	        } else if (event.target.id === 'editSidebar'){
	        	div.innerHTML = `<form name="editItems">
	        						<p>
		        						<label for="side_item-1">Change text for item 1</label>
		        						<input type="text" name="side_item-1" placeholder="${side_item1.innerText}">
		        						<label for="side_item-1">Change link for item 1</label>
		        						<input type="text" name="side_item-1-link" placeholder="${side_item1.querySelector("a").href}" data-id="link">
		        					</p>
									
									<p>
		        						<label for="side_item-2">Change text for item 2</label>
		        						<input type="text" name="side_item-2" placeholder="${side_item2.innerText}">
		        						<label for="side_item-2">Change link for item 2</label>
		        						<input type="text" name="side_item-2-link" placeholder="${side_item2.querySelector("a").href}" data-id="link">
									</p>
									<p>
		        						<label for="side_item-3">Change text for item 3</label>
		        						<input type="text" name="side_item-3" placeholder="${side_item3.innerText}">
		        						<label for="side_item-3">Change link for item 3</label>
		        						<input type="text" name="side_item-3-link" placeholder="${side_item3.querySelector("a").href}" data-id="link">
		        					</p>
		        					<p>
		        						<label for="side_item-4">Change text for item 4</label>
		        						<input type="text" name="side_item-4" placeholder="${side_item4.innerText}">
		        						<label for="side_item-4">Change link for item 4</label>
		        						<input type="text" name="side_item-4-link" placeholder="${side_item4.querySelector("a").href}" data-id="link">
		        					</p>
	        						<div class="buttons">
	        						<p>
	        							<a href="#" id="save" class="button save"/>SAVE</a>
	        							<a href="#" id="cancel" class="button cancel"/>CANCEL</a>
	        						</p>
	        						</div>
	        					</form>`
	        	content.appendChild(div);
	        }
	        //добавляем кнопки
	        const cancel = document.getElementById('cancel');
	        const save = document.getElementById('save');
	        cancel.addEventListener('click', (e) => {
	        	e.preventDefault();
		        fade.style.display = 'none';
		        fade.remove();
		    })
		    save.addEventListener('click', saveToLS);
		}
export default popup;