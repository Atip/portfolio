export let saveToLS = (e) =>{
			    	e.preventDefault();
			    	const form = document.forms.editItems; // выбираем форму
			    	const elems = form.elements;  // выбираем все элементы формы
			    	const elemsArray = Array.from(elems); // делаем из них массив, чтобы перебирать
			    	const fade = document.querySelector('.fade');

			    	// Записываем значения в ЛС
			    	elemsArray.forEach((item) => {
			    		if (item.value === ''){
							if (item.dataset.id === "link"){
				    			localStorage.setItem( item.name + "-link", item.placeholder );
				    		} else {
				    			localStorage.setItem( item.name, item.placeholder ); // перебираем значения и записываем в ЛС имя и значение	
				    		}    			
			    		} else {
				    		if (item.dataset.id === "link"){
				    			localStorage.setItem( item.name + "-link", item.value );
				    		} else {
				    			localStorage.setItem( item.name, item.value ); // перебираем значения и записываем в ЛС имя и значение	
				    		}
				    	}
			    		
			    	})
					
					//Генерируем верхний навбар
					const navbar = document.getElementById('navbar');
					const elemsNav = navbar.querySelectorAll('li');
					elemsNav.forEach((item) => {
						if (localStorage.getItem(item.dataset.name)) {
							item.innerHTML = `<a href="${localStorage.getItem(item.dataset.name + '-link-link')}">${localStorage.getItem(item.dataset.name)}</a>`;							
						};
					})

					//Генерируем сайдбар
					const sidebar = document.getElementById('sidebar');
					const elemsSide = sidebar.querySelectorAll('li');
					elemsSide.forEach((item) => {
						if (localStorage.getItem(item.dataset.name)) {
							item.innerHTML = `<a href="${localStorage.getItem(item.dataset.name + '-link-link')}">${localStorage.getItem(item.dataset.name)}</a>`;							
						};
					})

					// fade.style.display = 'none';
		        	fade.remove();	    	
				}

export default saveToLS;