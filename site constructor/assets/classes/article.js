export let articleArray = [];
export class Article {
			constructor(author, text, header, image){
				this.author = author;
				this.text = text;
				this.header = header;
				let d = new Date;
				this.date = d.toLocaleDateString('en-US');
				if (image === ''){
					this.image = this.image;
				} else {
					this.image = image;
				}
				if (localStorage.getItem('articles')){
					articleArray = JSON.parse(localStorage.getItem('articles'));
					articleArray.push(this);
				} else {
					articleArray.push(this);	
				}
				
			}			
		}
		Article.prototype.image = '../public/img/noimage.png';

export default {Article, articleArray};